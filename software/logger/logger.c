#include "sys/alt_timestamp.h"
#include "system.h"
#include <stdio.h>
#include <stdlib.h>
#include <altera_up_sd_card_avalon_interface.h>
#include "altera_up_avalon_video_pixel_buffer_dma.h"
#include "altera_up_avalon_video_character_buffer_with_dma.h"
#include "altera_up_avalon_audio_and_video_config.h"
#include "altera_up_avalon_audio.h"
#include "sys/alt_irq.h"
#include "altera_up_avalon_ps2.h"
#include "altera_up_ps2_keyboard.h"

#define AMP_HIGH 0x0000007F
#define AMP_LOW 0x0000FF80

#define E_LOW 659
#define G_LOW 784
#define A_FLAT 831
#define A_NOTE 880
#define B_FLAT 940//932
#define B_NOTE 988
#define C_NOTE 1047
#define D_NOTE 1175
#define E_FLAT 1245
#define E_NOTE 1319
#define F_NOTE 1397
#define G_FLAT 1480
#define G_NOTE 1568
#define A_HIGH 1760
#define C_HIGH 2093
#define SILENCE 0

#define QUARTER 265 // 2 ms iterations
#define HALF 2*QUARTER
#define WHOLE 4*QUARTER

const int FroggerWidth = 10;

alt_up_ps2_dev* ps2_device;

const int mario[] = {   E_NOTE, QUARTER,
                                                E_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                E_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                C_NOTE, QUARTER,
                                                E_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                G_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                SILENCE, QUARTER,
                                                SILENCE, QUARTER,
                                                G_LOW, QUARTER,
                                                SILENCE, QUARTER,
                                                SILENCE, QUARTER,
                                                SILENCE, QUARTER,
                                                C_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                SILENCE, QUARTER,
                                                G_LOW, QUARTER,
                                                SILENCE, QUARTER,
                                                SILENCE, QUARTER,
                                                E_LOW, QUARTER,
                                                SILENCE, QUARTER,
                                                SILENCE, QUARTER,
                                                A_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                B_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                B_FLAT, QUARTER,
                                                A_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                G_LOW, HALF,
                                                E_NOTE, HALF,
                                                G_NOTE, HALF,
                                                A_HIGH, QUARTER,
                                                SILENCE, QUARTER,
                                                F_NOTE, QUARTER,
                                                G_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                E_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                C_NOTE, QUARTER,
                                                D_NOTE, QUARTER,
                                                B_NOTE, QUARTER,
                                                SILENCE, HALF,
                                                C_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                SILENCE, QUARTER,
                                                G_LOW, QUARTER,
                                                SILENCE, QUARTER,
                                                SILENCE, QUARTER,
                                                E_LOW, QUARTER,
                                                SILENCE, QUARTER,
                                                SILENCE, QUARTER,
                                                A_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                B_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                B_FLAT, QUARTER,
                                                A_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                G_LOW, HALF,
                                                E_NOTE, HALF,
                                                G_NOTE, HALF,
                                                A_HIGH, QUARTER,
                                                SILENCE, QUARTER,
                                                F_NOTE, QUARTER,
                                                G_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                E_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                C_NOTE, QUARTER,
                                                D_NOTE, QUARTER,
                                                B_NOTE, QUARTER,
                                                SILENCE, HALF,
                                                SILENCE, HALF,
                                                G_NOTE, QUARTER,
                                                G_FLAT, QUARTER,
                                                E_NOTE, QUARTER,
                                                E_FLAT, QUARTER,
                                                SILENCE, QUARTER,
                                                E_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                A_FLAT, QUARTER,
                                                A_NOTE, QUARTER,
                                                C_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                A_NOTE, QUARTER,
                                                C_NOTE, QUARTER,
                                                D_NOTE, QUARTER,
                                                SILENCE, HALF,
                                                G_NOTE, QUARTER,
                                                G_FLAT, QUARTER,
                                                E_NOTE, QUARTER,
                                                E_FLAT, QUARTER,
                                                SILENCE, QUARTER,
                                                E_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                C_HIGH, QUARTER,
                                                SILENCE, QUARTER,
                                                C_HIGH, QUARTER,
                                                C_HIGH, QUARTER,
                                                SILENCE, QUARTER,
                                                SILENCE, HALF,
                                                SILENCE, HALF,
                                                G_NOTE, QUARTER,
                                                G_FLAT, QUARTER,
                                                E_NOTE, QUARTER,
                                                E_FLAT, QUARTER,
                                                SILENCE, QUARTER,
                                                E_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                A_FLAT, QUARTER,
                                                A_NOTE, QUARTER,
                                                C_NOTE, QUARTER,
                                                SILENCE, QUARTER,
                                                A_NOTE, QUARTER,
                                                C_NOTE, QUARTER,
                                                D_NOTE, QUARTER,
                                                SILENCE, HALF,
                                                E_FLAT, QUARTER,
                                                SILENCE, HALF,
                                                D_NOTE, HALF,
                                                SILENCE, HALF,
                                                C_NOTE, HALF,
                                                SILENCE, QUARTER,
                                                SILENCE, HALF,
                                                SILENCE, WHOLE
                                };
const int song_elements = 258;
int song_index = 0;
int loop_iterations = 0;

enum doNotToggle
{
	RowHeightPx = 10,
	MaxGameHeight = 24,
	ScreenWidthPx = 310,
	ScreenHeightPx = 240,
	LumberJackWidth = 320 - ScreenWidthPx,
	LogLength = 20,
	NumWaves = 3,
	WaveLen = 5,
	HighScoreListSize = 5,
	HighScoreNameSize = 3,
};

typedef enum Direction
{
	Direction_Up,
	Direction_Down,
	Direction_Left,
	Direction_Right
} Direction;

enum Colors
{
	Color_Frogger = 0x0F00,
	Color_Log = 0x5922,
	Color_Water = 0x00FF,
	Color_Grass = 0x3AE1,
	Color_Error = 0xF800,
	Color_Wave = 0xFFFF,
	Color_Platform = 0xFFF3,

		//lumberjack colors
	Color_Red = 0xF000,
	Color_Skin = 0xFEB0,
	Color_Eye = 0x0000,
	Color_Blue = 0x19F5,
	Color_Hair = 0x58E0,
};

typedef enum GameState
{
	GameStateWin,
	GameStateLose,
	GameStateInProgress
} GameState;

typedef struct HighScoreEntry
{
	int score;

	char* name;
	struct HighScoreEntry* nextEntry;
} HighScoreEntry;

typedef enum MenuOptions
{
	MenuTitle = 24,
	MenuPlayGame = 28,
	MenuLevelSelect = 30,
	MenuHighScores = 32,
} MenuOptions;

typedef enum LevelOptions
{
	Level1 = 24,
	Level2 = 26,
	Level3 = 28,
	Level4 = 30,
	Level5 = 32
} LevelOptions;

enum MenuLocations
{
	MenuCaretX = 32,
	MenuOptionX = 34
};

typedef struct Log {
	int startPixel;
	int endPixel;
	struct Log *nextLog;
} Log;

typedef struct Wave {
	int startPixel;
} Wave;

typedef struct LumberJack{
	int currentRow;
	int previousRow;

	//Button memory
	int upPressed;
	int downPressed;
	int logPressed;
} LumberJack;

typedef struct {
	int speed;
	int strafeTarget;
	Log *logList;
	Wave waveList[5];

} Row;

typedef struct Frogger
{
	unsigned int row;
	int leftPixel;
} Frogger;

enum ToggleThese
{
	GameHeight = MaxGameHeight,
	GameLoopTimeMs = 1
};

//Speeds of logs on rows
//+ logs flow left to right
//- logs flow right to left
//0 is land
const int RiverSpeeds[GameHeight] = {0,0,0,1,2,3,0,1,2,0,1,-1,3,-1,0,1,0,0,0,0};

Row GameBoard[GameHeight];

int remainingAmmo = 0;

//Function Prototypes
int sign(int x);
void drawFroggerShape(alt_up_pixel_buffer_dma_dev* pxBuffer, Frogger* frogger, unsigned int color);
void addLog(int row, int size);
void drawCellPixel(alt_up_pixel_buffer_dma_dev* pxBuffer, unsigned int color, int x, int row);
void drawGameBoard(alt_up_pixel_buffer_dma_dev* pxBuffer);
void initializeGame(alt_up_char_buffer_dev *char_buffer, alt_up_pixel_buffer_dma_dev* pxBuffer, Frogger * frogger, LumberJack * player, int level[], int logAmmo);
int isButtonActive(int num);
void drawLumberJack(alt_up_pixel_buffer_dma_dev* pxBuffer, LumberJack *player);
void eraseLumberJack(alt_up_pixel_buffer_dma_dev* pxBuffer, LumberJack *player);
void checkButtons(alt_up_char_buffer_dev *char_buffer, alt_up_pixel_buffer_dma_dev* pxBuffer, LumberJack *player);
void drawWave(alt_up_pixel_buffer_dma_dev* pxBuffer, unsigned int color, int row, int startPixel);
void eraseWaveCheck(alt_up_pixel_buffer_dma_dev* pxBuffer, int row, int startPixel);
void drawWaveCheck(alt_up_pixel_buffer_dma_dev* pxBuffer, int row, int startPixel);
void updateWaves(alt_up_pixel_buffer_dma_dev* pxBuffer, alt_up_audio_dev * audio);
void updateLogs(alt_up_pixel_buffer_dma_dev* pxBuffer, LumberJack* player, alt_up_audio_dev * audio);
int froggerCanJumpTo(int row, int xPixel);
int froggerCanMove(Frogger* frogger, Direction direction);
int froggerWantsToAdvance(Frogger* frogger);
void froggerStrafeTo(Frogger* frogger, int xGoal);
void froggerAiMovement(Frogger* frogger);
void updateFrogger(alt_up_pixel_buffer_dma_dev* pxBuffer, Frogger* frogger);
alt_up_pixel_buffer_dma_dev* initVGA();
GameState checkGameState(Frogger* frogger, alt_up_char_buffer_dev *char_buffer);
void clearLogs(void);void av_config_setup(void);
void timerDelay(alt_up_audio_dev * audio, float MenuLoopClockTickCount);
int playGame(alt_up_char_buffer_dev *char_buffer, alt_up_pixel_buffer_dma_dev* pxBuffer, alt_up_audio_dev * audio, int level[], int logAmmo, float GameLoopClockTickCount);
void highscores(alt_up_char_buffer_dev *char_buffer, alt_up_pixel_buffer_dma_dev* pxBuffer);
void showMenu(alt_up_char_buffer_dev *char_buffer, alt_up_pixel_buffer_dma_dev* pxBuffer, alt_up_audio_dev * audio, float MenuLoopClockTickCount);
int draw_pixel_fast(alt_up_pixel_buffer_dma_dev *pixel_buffer, unsigned int color, unsigned int x, unsigned int y);
void refillAudioBuffer(alt_up_audio_dev * audio);
void readLevel(LevelOptions currentLevelOption, int level[], int * logAmmo);
void initializeAmmo(alt_up_char_buffer_dev *char_buffer);
void updateAmmo(alt_up_char_buffer_dev *char_buffer);
char get_key_from_keyboard(void);
HighScoreEntry* loadHighScores();
void printHighScores(alt_up_char_buffer_dev *charBuffer, HighScoreEntry* highScores, float MenuLoopClockTickCount,LevelOptions currentLevelOption);
char* getPlayerName(alt_up_char_buffer_dev *charBuffer);
void saveHighScores(HighScoreEntry *head, LevelOptions currentLevelOption);
void releaseHighScores(HighScoreEntry* head);
HighScoreEntry* recalculateHighScores(alt_up_char_buffer_dev* charBuffer, HighScoreEntry *highScores, int playerScore, LevelOptions currentLevelOption);
int draw_pixel_fast(alt_up_pixel_buffer_dma_dev *pixel_buffer,
		unsigned int color, unsigned int x, unsigned int y) {
	unsigned int addr;

	addr = ((x & pixel_buffer->x_coord_mask) << 1);
	addr += (((y & pixel_buffer->y_coord_mask) * 320) << 1);

	IOWR_16DIRECT(pixel_buffer->back_buffer_start_address, addr, color);

	return 0;
}

int abs(int x)
{
	if (x < 0)
		return -x;
	else
		return x;
}

int sign(int x)
{
	if (x > 0)
		return 1;
	else if (x < 0)
		return -1;
	else
		return 0;
}

void drawLumberJack(alt_up_pixel_buffer_dma_dev* pxBuffer, LumberJack *player)
{
	//Draw new position
	int y = player->currentRow * RowHeightPx;
	int x = ScreenWidthPx;
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+3);
	++x;
	draw_pixel_fast(pxBuffer, Color_Red, x, y+1);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+3);
	draw_pixel_fast(pxBuffer, Color_Hair, x, y+4);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+7);
	++x;
	draw_pixel_fast(pxBuffer, Color_Red, x, y+1);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+2);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+3);
	draw_pixel_fast(pxBuffer, Color_Hair, x, y+4);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+5);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+7);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+9);
	++x;
	draw_pixel_fast(pxBuffer, Color_Red, x, y+1);
	draw_pixel_fast(pxBuffer, Color_Eye, x, y+2);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+3);
	draw_pixel_fast(pxBuffer, Color_Hair, x, y+4);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+5);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+6);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+7);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+8);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+9);
	++x;
	draw_pixel_fast(pxBuffer, Color_Red, x, y);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+1);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+2);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+3);
	draw_pixel_fast(pxBuffer, Color_Hair, x, y+4);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+5);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+6);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+7);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+8);
	++x;
	draw_pixel_fast(pxBuffer, Color_Red, x, y);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+1);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+2);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+3);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+4);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+5);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+6);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+7);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+8);
	++x;
	draw_pixel_fast(pxBuffer, Color_Red, x, y);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+1);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+2);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+3);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+4);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+5);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+6);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+7);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+8);
	++x;
	draw_pixel_fast(pxBuffer, Color_Red, x, y);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+1);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+2);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+3);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+4);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+5);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+6);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+7);
	draw_pixel_fast(pxBuffer, Color_Blue, x, y+8);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+9);
	++x;
	draw_pixel_fast(pxBuffer, Color_Red, x, y);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+1);
	draw_pixel_fast(pxBuffer, Color_Hair, x, y+2);
	draw_pixel_fast(pxBuffer, Color_Hair, x, y+3);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+4);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+5);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+7);
	draw_pixel_fast(pxBuffer, Color_Red, x, y+9);
	++x;
	draw_pixel_fast(pxBuffer, Color_Red, x, y+1);
	draw_pixel_fast(pxBuffer, Color_Hair, x, y+2);
	draw_pixel_fast(pxBuffer, Color_Hair, x, y+3);
	draw_pixel_fast(pxBuffer, Color_Hair, x, y+4);
	draw_pixel_fast(pxBuffer, Color_Skin, x, y+7);
}

void eraseLumberJack(alt_up_pixel_buffer_dma_dev* pxBuffer, LumberJack *player)
{
	//Erase old position
	int i;
	for(i=ScreenWidthPx;i<(ScreenWidthPx+LumberJackWidth);i++)
	{
		drawCellPixel(pxBuffer,Color_Platform,i,player->previousRow);
	}

}

void drawFroggerShape(alt_up_pixel_buffer_dma_dev* pxBuffer, Frogger* frogger, unsigned int color)
{
	int y = frogger->row * RowHeightPx;
	int xPixel = frogger->leftPixel;
	//draw columns....
	if (xPixel >= 0)
	{
		draw_pixel_fast(pxBuffer, color, xPixel, y+1);
		draw_pixel_fast(pxBuffer, color, xPixel, y+8);
	}
	if (++xPixel >= ScreenWidthPx)
		return;
	else if (xPixel >= 0)
	{
		draw_pixel_fast(pxBuffer, color, xPixel, y+0);
		draw_pixel_fast(pxBuffer, color, xPixel, y+1);
		draw_pixel_fast(pxBuffer, color, xPixel, y+2);
		draw_pixel_fast(pxBuffer, color, xPixel, y+3);
		draw_pixel_fast(pxBuffer, color, xPixel, y+7);
		draw_pixel_fast(pxBuffer, color, xPixel, y+8);
		draw_pixel_fast(pxBuffer, color, xPixel, y+9);
	}
	if (++xPixel >= ScreenWidthPx)
		return;
	else if (xPixel >= 0)
	{
		draw_pixel_fast(pxBuffer, color, xPixel, y+3);
		draw_pixel_fast(pxBuffer, color, xPixel, y+6);
		draw_pixel_fast(pxBuffer, color, xPixel, y+7);
	}
	if (++xPixel >= ScreenWidthPx)
		return;
	else if (xPixel >= 0)
	{
		draw_pixel_fast(pxBuffer, color, xPixel, y+2);
		draw_pixel_fast(pxBuffer, color, xPixel, y+3);
		draw_pixel_fast(pxBuffer, color, xPixel, y+4);
		draw_pixel_fast(pxBuffer, color, xPixel, y+5);
		draw_pixel_fast(pxBuffer, color, xPixel, y+6);
	}
	if (++xPixel >= ScreenWidthPx)
		return;
	else if (xPixel >= 0)
	{
		draw_pixel_fast(pxBuffer, color, xPixel, y+1);
		draw_pixel_fast(pxBuffer, color, xPixel, y+2);
		draw_pixel_fast(pxBuffer, color, xPixel, y+3);
		draw_pixel_fast(pxBuffer, color, xPixel, y+4);
		draw_pixel_fast(pxBuffer, color, xPixel, y+5);
		draw_pixel_fast(pxBuffer, color, xPixel, y+6);
	}
	if (++xPixel >= ScreenWidthPx)
		return;
	else if (xPixel >= 0)
	{
		draw_pixel_fast(pxBuffer, color, xPixel, y+1);
		draw_pixel_fast(pxBuffer, color, xPixel, y+2);
		draw_pixel_fast(pxBuffer, color, xPixel, y+3);
		draw_pixel_fast(pxBuffer, color, xPixel, y+4);
		draw_pixel_fast(pxBuffer, color, xPixel, y+5);
		draw_pixel_fast(pxBuffer, color, xPixel, y+6);
	}
	if (++xPixel >= ScreenWidthPx)
		return;
	else if (xPixel >= 0)
	{
		draw_pixel_fast(pxBuffer, color, xPixel, y+2);
		draw_pixel_fast(pxBuffer, color, xPixel, y+3);
		draw_pixel_fast(pxBuffer, color, xPixel, y+4);
		draw_pixel_fast(pxBuffer, color, xPixel, y+5);
		draw_pixel_fast(pxBuffer, color, xPixel, y+6);
	}
	if (++xPixel >= ScreenWidthPx)
		return;
	else if (xPixel >= 0)
	{
		draw_pixel_fast(pxBuffer, color, xPixel, y+3);
		draw_pixel_fast(pxBuffer, color, xPixel, y+6);
		draw_pixel_fast(pxBuffer, color, xPixel, y+7);
	}
	if (++xPixel >= ScreenWidthPx)
		return;
	else if (xPixel >= 0)
	{
		draw_pixel_fast(pxBuffer, color, xPixel, y+0);
		draw_pixel_fast(pxBuffer, color, xPixel, y+1);
		draw_pixel_fast(pxBuffer, color, xPixel, y+2);
		draw_pixel_fast(pxBuffer, color, xPixel, y+3);
		draw_pixel_fast(pxBuffer, color, xPixel, y+7);
		draw_pixel_fast(pxBuffer, color, xPixel, y+8);
		draw_pixel_fast(pxBuffer, color, xPixel, y+9);
	}
	if (++xPixel >= ScreenWidthPx)
		return;
	else if (xPixel >= 0)
	{
		draw_pixel_fast(pxBuffer, color, xPixel, y+1);
		draw_pixel_fast(pxBuffer, color, xPixel, y+8);
	}
}

void initializeAmmo(alt_up_char_buffer_dev *char_buffer)
{
	char logStr[19];
	sprintf(logStr,"Logs Remaining: %d",remainingAmmo);
	alt_up_char_buffer_string(char_buffer,logStr, 1, 58);
}
void updateAmmo(alt_up_char_buffer_dev *char_buffer)
{
	char logStr[2];
	sprintf(logStr,"%d",remainingAmmo);
	alt_up_char_buffer_string(char_buffer, " ", 17, 58);
	alt_up_char_buffer_string(char_buffer, " ", 18, 58);
	alt_up_char_buffer_string(char_buffer,logStr, 17, 58);
}

void addLog(int row, int size)
{
	//Check if log should extend
	if(GameBoard[row].logList != NULL && GameBoard[row].speed > 0 && GameBoard[row].logList->endPixel <= 0)
	{
		GameBoard[row].logList->endPixel -= size;
		return;
	}

	if(GameBoard[row].logList != NULL && GameBoard[row].speed < 0 && GameBoard[row].logList->endPixel >= ScreenWidthPx-1)
	{
		GameBoard[row].logList->endPixel += size;
		return;
	}

	Log *newLog = (Log *)malloc(sizeof(Log));

	if(GameBoard[row].speed > 0)
	{
		newLog->startPixel = -GameBoard[row].speed;
		newLog->endPixel = -size - GameBoard[row].speed;
	}
	else
	{
		newLog->startPixel = (ScreenWidthPx-1) - GameBoard[row].speed;
		newLog->endPixel = (ScreenWidthPx-1) - GameBoard[row].speed + size;
	}

	newLog->nextLog = GameBoard[row].logList;
	GameBoard[row].logList = newLog;
}

void drawCellPixel(alt_up_pixel_buffer_dma_dev* pxBuffer, unsigned int color, int x, int row)
{
	int i;
	int y = row * RowHeightPx;
	for(i = 0; i < RowHeightPx; ++y, ++i)
	{
		draw_pixel_fast(pxBuffer, color, x, y);
	}
}

void drawGameBoard(alt_up_pixel_buffer_dma_dev* pxBuffer)
{
	int h, w;
	unsigned int color;
	for (h = 0; h < GameHeight; ++h)
	{
		switch(GameBoard[h].speed)
		{
		case 0: 	color = Color_Grass;
					break;
		default: 	color = Color_Water;
		}
		for (w = 0; w < ScreenWidthPx; ++w)
		{
			drawCellPixel(pxBuffer, color, w, h);
		}
	}
}

void drawPlatform(alt_up_pixel_buffer_dma_dev* pxBuffer)
{
	int i,j;
	for(i=0;i<GameHeight;i++)
	{
		for(j=ScreenWidthPx;j<(ScreenWidthPx+LumberJackWidth);j++)
		{
			drawCellPixel(pxBuffer, Color_Platform, j, i);
		}
	}
}

void initializeGame(alt_up_char_buffer_dev *char_buffer, alt_up_pixel_buffer_dma_dev* pxBuffer, Frogger * frogger, LumberJack * player, int level[], int logAmmo)
{
	alt_timestamp_start();
	remainingAmmo = logAmmo;
	initializeAmmo(char_buffer);
	int h, i;
	for (h = 0; h < GameHeight; ++h)
	{
		GameBoard[h].speed = level[h];
		if (GameBoard[h].speed == 0)
		{
			GameBoard[h].strafeTarget = (ScreenWidthPx - FroggerWidth) / 2;
		}
		else if (GameBoard[h].speed < 0)
		{
			GameBoard[h].strafeTarget = (1 - GameBoard[h].speed) * (ScreenWidthPx - FroggerWidth) / (2 - GameBoard[h].speed);
		}
		else
		{
			GameBoard[h].strafeTarget = (ScreenWidthPx - FroggerWidth) / (2 + GameBoard[h].speed);
		}
		GameBoard[h].logList = NULL;
		if(GameBoard[h].speed != 0)
		{
			GameBoard[h].waveList[0].startPixel = 100;
			GameBoard[h].waveList[1].startPixel = 200;
			GameBoard[h].waveList[2].startPixel = 300;
		}

	}

	drawGameBoard(pxBuffer);
	drawPlatform(pxBuffer);
	srand(alt_timestamp());

	//Initializing LumberJack
	for(i=0;i<GameHeight;i++)
	{
		if(GameBoard[i].speed != 0)
		{
			player->previousRow = i;
			player->currentRow = i;
			break;
		}
	}
	player->upPressed = 0;
	player->downPressed = 0;
	player->logPressed = 0;

	//Initialize frogger
	frogger->row = GameHeight - 1;
	frogger->leftPixel = ScreenWidthPx / 4;

	drawLumberJack(pxBuffer, player);
}

int isButtonActive(int num)
{
	if (num == 0)
		return ((~*(int *)BUTTONS_BASE) & 0x01) || get_key_from_keyboard() == 'X';
	else if (num == 1)
		return ((~*(int *)BUTTONS_BASE) & 0x02) || get_key_from_keyboard() == 'A' || get_key_from_keyboard() == 'D';
	else if (num == 2)
		return ((~*(int *)BUTTONS_BASE) & 0x04) || get_key_from_keyboard() == 'S';
	else if (num == 3)
		return ((~*(int *)BUTTONS_BASE) & 0x08) || get_key_from_keyboard() == 'W';
	else
		return 0;
}

char get_key_from_keyboard() {
	KB_CODE_TYPE decode_mode;
	alt_u8 make_code[4];
	char ascii_buffer;
	decode_scancode(ps2_device, &decode_mode, make_code, &ascii_buffer);
	// printf("make code: %x, ascii: %x \n", make_code, ascii_buffer);

	return ascii_buffer;
}

void checkButtons(alt_up_char_buffer_dev *char_buffer, alt_up_pixel_buffer_dma_dev* pxBuffer, LumberJack *player)
{
	int i;
	//Up
	if(isButtonActive(3))
	{
		if(player->upPressed == 0)
		{
			player->upPressed = 1;
			for(i=(player->currentRow)-1;i>=0;i--)
			{
				if(GameBoard[i].speed != 0)
				{
					player->previousRow = player->currentRow;
					player->currentRow = i;
					drawLumberJack(pxBuffer, player);
					eraseLumberJack(pxBuffer, player);
					break;
				}
			}
		}
	}else{
		player->upPressed = 0;
	}

	//Down
	if(isButtonActive(2))
	{
		if(player->downPressed == 0)
		{
			player->downPressed = 1;
			for(i=(player->currentRow)+1;i<GameHeight;i++)
			{
				if(GameBoard[i].speed != 0)
				{
					player->previousRow = player->currentRow;
					player->currentRow = i;
					drawLumberJack(pxBuffer, player);
					eraseLumberJack(pxBuffer, player);
					break;
				}
			}
		}
	}else{
		player->downPressed = 0;
	}

	//Release
	if(isButtonActive(1))
	{
		if(player->logPressed == 0 && remainingAmmo > 0)
		{
			player->logPressed = 1;
			addLog(player->currentRow, LogLength);
			remainingAmmo--;
			updateAmmo(char_buffer);
		}
	}else{
		player->logPressed = 0;
	}
}

int doesLogExistAt(int row, int startPixel, int endPixel)
{
	Log *head = GameBoard[row].logList;
	if(GameBoard[row].speed > 0)
	{
		while(head != NULL)
		{
			if((startPixel >= head->endPixel  && startPixel <= head->startPixel) || (endPixel >= head->endPixel && endPixel <= head->startPixel))
				return 1;

			head = head->nextLog;
		}
	}
	else
	{
		while(head != NULL)
		{
			if((startPixel <= head->endPixel  && startPixel >= head->startPixel) || (endPixel <= head->endPixel && endPixel >= head->startPixel))
				return 1;

			head = head->nextLog;
		}
	}

	return 0;

}

void drawWave(alt_up_pixel_buffer_dma_dev* pxBuffer, unsigned int color, int row, int startPixel)
{
	int i;
	int y = (row * RowHeightPx) + (RowHeightPx/2);

	if(GameBoard[row].speed > 0)
	{
		for(i = 0; i < WaveLen; i++)
		{
			if(startPixel-i < ScreenWidthPx && startPixel-i >= 0)
			{
				draw_pixel_fast(pxBuffer, color, startPixel-i, ((i/2)%2 == 0) ? y-(i%2): y-(2-(i%2)));
			}
		}
	}
	else
	{
		for(i = 0; i < WaveLen; i++)
		{
			if(startPixel+i < ScreenWidthPx && startPixel+i >= 0)
			{
				draw_pixel_fast(pxBuffer, color, startPixel+i, ((i/2)%2 == 0) ? y-(i%2): y-(2-(i%2)));
			}
		}
	}
}

void eraseWaveCheck(alt_up_pixel_buffer_dma_dev* pxBuffer, int row, int startPixel)
{
	if(GameBoard[row].speed > 0)
	{
		if(doesLogExistAt(row, startPixel, startPixel-WaveLen) != 1)
		{
			//Erase Wave
			drawWave(pxBuffer, Color_Water, row, startPixel);
		}
	}
	else
	{
		if(doesLogExistAt(row, startPixel, startPixel+WaveLen) != 1)
		{
			//Erase Wave
			drawWave(pxBuffer, Color_Water, row, startPixel);
		}
	}
}

void drawWaveCheck(alt_up_pixel_buffer_dma_dev* pxBuffer, int row, int startPixel)
{
	if(GameBoard[row].speed > 0)
	{
		if(doesLogExistAt(row, startPixel, startPixel-WaveLen-1) != 1)
		{
			//Erase Wave
			drawWave(pxBuffer, Color_Wave, row, startPixel);
		}
	}
	else
	{
		if(doesLogExistAt(row, startPixel, startPixel+WaveLen+1) != 1)
		{
			//Erase Wave
			drawWave(pxBuffer, Color_Wave, row, startPixel);
		}
	}
}

void updateWaves(alt_up_pixel_buffer_dma_dev* pxBuffer, alt_up_audio_dev * audio)
{
	int i,j;
	for(i = 0; i < GameHeight; i++)
	{
		refillAudioBuffer(audio);
		if(GameBoard[i].speed == 0)
			continue;


		for(j = 0; j < NumWaves; j++)
		{
			eraseWaveCheck(pxBuffer, i, GameBoard[i].waveList[j].startPixel);
			GameBoard[i].waveList[j].startPixel += GameBoard[i].speed;

			//Check for edge cases
			if(GameBoard[i].speed > 0 && GameBoard[i].waveList[j].startPixel-WaveLen >= ScreenWidthPx)
				GameBoard[i].waveList[j].startPixel = 0;

			if(GameBoard[i].speed < 0 && GameBoard[i].waveList[j].startPixel+WaveLen < 0)
				GameBoard[i].waveList[j].startPixel = ScreenWidthPx-1;


			drawWaveCheck(pxBuffer, i, GameBoard[i].waveList[j].startPixel);
		}
	}
}

void updateLogs(alt_up_pixel_buffer_dma_dev* pxBuffer, LumberJack* player, alt_up_audio_dev * audio)
{
	int i, j;
	for(i = 0; i < GameHeight; i++)
	{
		refillAudioBuffer(audio);
		Log **prev = &(GameBoard[i].logList);
		Log *Head = GameBoard[i].logList;

		while(Head != NULL)
		{
			if(GameBoard[i].speed == 0)
				continue;

			Head->startPixel += GameBoard[i].speed;
			Head->endPixel += GameBoard[i].speed;

			//Draw trailing water
			for (j = 0; j != GameBoard[i].speed; j += sign(GameBoard[i].speed))
			{
				if ((Head->endPixel - j < 0) ||
					(Head->endPixel - j >= ScreenWidthPx) )
					continue;

				drawCellPixel(pxBuffer, Color_Water, Head->endPixel-j, i);
			}

			//Free the log
			if((GameBoard[i].speed > 0 && Head->endPixel >= ScreenWidthPx) ||
					(GameBoard[i].speed < 0 && Head->endPixel < 0))
			{
				free(Head);
				(*prev) = NULL;
				Head = NULL;
				break;
			}

			//Draw Leading Log
			for (j = 0; j != GameBoard[i].speed; j += sign(GameBoard[i].speed))
			{
				if ((Head->startPixel - j < 0) ||
					(Head->startPixel - j >= ScreenWidthPx) )
					continue;

				drawCellPixel(pxBuffer, Color_Log, Head->startPixel-j, i);
			}
			//Draw trailing water
			for (j = 0; j != GameBoard[i].speed; j += sign(GameBoard[i].speed))
			{
				if ((Head->endPixel - j < 0) ||
					(Head->endPixel - j >= ScreenWidthPx) )
					continue;

				drawCellPixel(pxBuffer, Color_Water, Head->endPixel-j, i);
			}

			//Iterate through the List
			prev = &(Head->nextLog);
			Head = Head->nextLog;
		}
	}

	drawLumberJack(pxBuffer, player);
}

int froggerCanJumpTo(int row, int xPixel)
{
	if (	(xPixel < 0)
		|| 	(xPixel > ScreenWidthPx - FroggerWidth)
		||	(row < 0)
		||	(row >= GameHeight)
		)
		return 0;
	if (GameBoard[row].speed == 0)
		return 1;

	Log* log = GameBoard[row].logList;
	while (log != NULL)
	{
		if (GameBoard[row].speed > 0)
		{
			//startpixel is on the right
			if (	(xPixel + FroggerWidth < log->startPixel)
				&&	(xPixel > log->endPixel + GameBoard[row].speed)
				)
			{
				return 1;
			}
			else if (xPixel > log->startPixel)
			{
				log = log->nextLog;
				continue;
			}
			else
			{
				break;
			}
		}
		else if (GameBoard[row].speed < 0)
		{
			//startpixel is on the left
			if (	(xPixel > log->startPixel)
				&&	(xPixel + FroggerWidth <= log->endPixel + GameBoard[row].speed)
				)
			{
				return 1;
			}
			else if (xPixel < log->startPixel)
			{
				log = log->nextLog;
				continue;
			}
			else
			{
				break;
			}
		}
	}
	return 0;
}

int froggerCanMove(Frogger* frogger, Direction direction)
{
	switch (direction)
	{
	case Direction_Left:
		return	( froggerCanJumpTo(frogger->row, frogger->leftPixel - 1) );
	case Direction_Right:
		return	( froggerCanJumpTo(frogger->row, frogger->leftPixel + 1) );
	case Direction_Up:
		return	( froggerCanJumpTo(frogger->row - 1, frogger->leftPixel) );
	case Direction_Down:
		return	( froggerCanJumpTo(frogger->row + 1, frogger->leftPixel) );
	}
	return 0;
}

int froggerWantsToAdvance(Frogger* frogger)
{
	return 1;
	if (GameBoard[frogger->row].speed != 0)
		return rand()% 100 > 90;
	else
		return rand()%100 > 70;
}

void froggerStrafeTo(Frogger* frogger, int xGoal)
{
	if (	(frogger->leftPixel > xGoal)
		&&	(froggerCanMove(frogger, Direction_Left))
		)
	{
		--frogger->leftPixel;
	}
	else if (	(frogger->leftPixel < xGoal)
		&&	(froggerCanMove(frogger, Direction_Left))
		)
	{
		++frogger->leftPixel;
	}
}

void froggerAiMovement(Frogger* frogger)
{
	const int EdgeHazardWidth = 40;

	int travelDistance = GameBoard[frogger->row - 1].strafeTarget - frogger->leftPixel;
	int froggerStrafeDistance = 0;

	if (froggerWantsToAdvance(frogger) && froggerCanMove(frogger, Direction_Up))
		--frogger->row;

	if ( abs(travelDistance) > 5)
		froggerStrafeDistance += 3*sign(travelDistance);

		//right edge of screen!
	if (frogger->leftPixel > ScreenWidthPx - FroggerWidth - EdgeHazardWidth)
	{
		if (froggerCanMove(frogger, Direction_Up))
			--frogger->row;
		else if (froggerCanMove(frogger, Direction_Left))
			--frogger->leftPixel;
		else if (froggerCanMove(frogger, Direction_Down))
			++frogger->row;
	}

		//left edge of screen!
	else if (frogger->leftPixel < EdgeHazardWidth)
	{
		if (froggerCanMove(frogger, Direction_Up))
			--frogger->row;
		else if (froggerCanMove(frogger, Direction_Right))
			++frogger->leftPixel;
		else if (froggerCanMove(frogger, Direction_Down))
			++frogger->row;
	}

	if (froggerCanJumpTo(frogger->row, frogger->leftPixel + froggerStrafeDistance))
	{
		frogger->leftPixel += froggerStrafeDistance;
	}
}

void updateFrogger(alt_up_pixel_buffer_dma_dev* pxBuffer, Frogger* frogger)
{
		//erase frogger!
	drawFroggerShape(pxBuffer, frogger, GameBoard[frogger->row].speed ? Color_Log : Color_Grass);

		//account for river drift
	frogger->leftPixel += GameBoard[frogger->row].speed;

		//froggre decides to think about it
	froggerAiMovement(frogger);

		//draw new frogger!
	drawFroggerShape(pxBuffer, frogger, Color_Frogger);
}
alt_up_pixel_buffer_dma_dev* initVGA()
{
        alt_up_pixel_buffer_dma_dev* pixel_buffer;
        // Use the name of your pixel buffer DMA core
        pixel_buffer = alt_up_pixel_buffer_dma_open_dev(PIXEL_BUFFER_DMA_NAME);
        // Set the background buffer address ñ Although we donít use the background,
        // they only provide a function to change the background buffer address, so
        // we must set that, and then swap it to the foreground.
        alt_up_pixel_buffer_dma_change_back_buffer_address(pixel_buffer, PIXEL_BUFFER_BASE);
        // Swap background and foreground buffers
        alt_up_pixel_buffer_dma_swap_buffers(pixel_buffer);
        // Wait for the swap to complete
        while
                (alt_up_pixel_buffer_dma_check_swap_buffers_status(pixel_buffer));
        // Clear the screen
        alt_up_pixel_buffer_dma_clear_screen(pixel_buffer, 0);
        return pixel_buffer;
}

int gameIsUnfinishable(Frogger* frogger)
{
	if (remainingAmmo == 0)
	{
		int i;
		for (i = frogger->row; i >= 0; i--)
		{
			if (	(GameBoard[i].speed == 0)
				||	(GameBoard[i].logList != NULL)
				)
				continue;
			return 1;
		}
	}
	return 0;
}

GameState checkGameState(Frogger* frogger, alt_up_char_buffer_dev *char_buffer)
{
	if (frogger->row == 0)
	{
		//victory
		alt_up_char_buffer_string(char_buffer, "VICTORY", 40, 30);
		return GameStateWin;

	}
	else if (	(frogger->leftPixel < 0)
			|| 	(frogger->leftPixel > ScreenWidthPx - FroggerWidth)
			|| 	(gameIsUnfinishable(frogger))
			)
	{
		//GG, lose a life and reset
		alt_up_char_buffer_string(char_buffer, "You Lose!", 40, 30);
		alt_timestamp_start();
		while ((float) (alt_timestamp()) < alt_timestamp_freq());

		return GameStateLose;
	}
	return GameStateInProgress;
}

void clearLogs(void)
{
	int i;
	for(i=0;i<RowHeightPx;i++)
	{
		Log *prev = GameBoard[i].logList;
		Log *head = GameBoard[i].logList;
		while(head != NULL)
		{
			head = prev->nextLog;
			free(prev);
			prev = head;
		}
	}
}

void addToBuffer(alt_up_audio_dev * audio, int frequency) {
        unsigned int period, high, low;
        if (frequency == SILENCE) {
                period = 2;
                high = 0;
                low = 0;
        }
        else {
                period = 32000 / frequency; // 64
                high = AMP_HIGH;
                low = AMP_LOW;
        }

        unsigned int buffer[period];

        int i;
        int halfPeriod = period/2;
        for (i = 0; i < halfPeriod; i++) {
                buffer[i] = AMP_HIGH;
        }
        for (i = halfPeriod; i < period; i++) {
                buffer[i] = AMP_LOW;
        }

        while (alt_up_audio_write_fifo_space(audio, ALT_UP_AUDIO_LEFT) >= period) {
                alt_up_audio_write_fifo(audio, buffer, period, ALT_UP_AUDIO_LEFT);
                alt_up_audio_write_fifo(audio, buffer, period, ALT_UP_AUDIO_RIGHT);
        }
}

void refillAudioBuffer(alt_up_audio_dev * audio) {
        if (loop_iterations < mario[song_index+1]) {
                loop_iterations++;
                addToBuffer(audio, mario[song_index]);
        }
        else {
                song_index += 2;
                loop_iterations = 0;
        }
        if (song_index > song_elements - 1)
                song_index = 0;
}

void timerDelay(alt_up_audio_dev * audio, float GameLoopClockTickCount)
{
        int framesPassed = 0;
        while (++framesPassed < 30)
        {
                refillAudioBuffer(audio);
                while ((float) (alt_timestamp()) < GameLoopClockTickCount);
                alt_timestamp_start();
        }
}

int playGame(alt_up_char_buffer_dev *char_buffer, alt_up_pixel_buffer_dma_dev* pxBuffer, alt_up_audio_dev * audio, int level[], int logAmmo, float GameLoopClockTickCount)
{
	alt_up_char_buffer_clear(char_buffer);
	LumberJack player;
	Frogger frogger;
	GameState currentGameState;
	initializeGame(char_buffer, pxBuffer, &frogger, &player, level, logAmmo);

	while(1)
	{
		checkButtons(char_buffer,pxBuffer,&player);
		updateLogs(pxBuffer, &player, audio);
		updateWaves(pxBuffer, audio);
		updateFrogger(pxBuffer, &frogger);
		currentGameState = checkGameState(&frogger, char_buffer);
		if(currentGameState != GameStateInProgress)
		{
			clearLogs();

				//wait for player to hit button one to continue back to main menu
			while(isButtonActive(1));
			while(!isButtonActive(1));

			if (currentGameState == GameStateWin)
				return remainingAmmo;
			else if (currentGameState == GameStateLose)
				return -1;
		}
		timerDelay(audio, GameLoopClockTickCount);
	}
}

void av_config_setup() {
        alt_up_av_config_dev * av_config = alt_up_av_config_open_dev(AV_CONFIG_NAME);
        while (!alt_up_av_config_read_ready(av_config)) {}
}

void printMenuOptions(alt_up_char_buffer_dev* charBuffer, MenuOptions currentMenuSelect)
{
    alt_up_char_buffer_clear(charBuffer);
    alt_up_char_buffer_string(charBuffer, "Logger Lite!", MenuOptionX, MenuTitle);
	alt_up_char_buffer_string(charBuffer, "Play Game", MenuOptionX, MenuPlayGame);
	alt_up_char_buffer_string(charBuffer, "Level Select", MenuOptionX, MenuLevelSelect);
	alt_up_char_buffer_string(charBuffer, "High Scores", MenuOptionX, MenuHighScores);
	alt_up_char_buffer_string(charBuffer, ">", MenuCaretX, currentMenuSelect);
}

void printLevelSelect(alt_up_char_buffer_dev* charBuffer, LevelOptions currentLevelSelect)
{
    alt_up_char_buffer_clear(charBuffer);
	alt_up_char_buffer_string(charBuffer, "Level 1", MenuOptionX, Level1);
	alt_up_char_buffer_string(charBuffer, "Level 2", MenuOptionX, Level2);
	alt_up_char_buffer_string(charBuffer, "Level 3", MenuOptionX, Level3);
	alt_up_char_buffer_string(charBuffer, "Level 4", MenuOptionX, Level4);
	alt_up_char_buffer_string(charBuffer, "Level 5", MenuOptionX, Level5);
	alt_up_char_buffer_string(charBuffer, ">", MenuCaretX, currentLevelSelect);
}

LevelOptions changeLevelOption(alt_up_char_buffer_dev* charBuffer, LevelOptions currentOption, int direction)
{
	alt_up_char_buffer_string(charBuffer, " ", MenuCaretX, currentOption);
	LevelOptions newOption = Level1;
	if (direction < 0)
	{
		switch (currentOption)
		{
		case Level1:	newOption = Level2;
						break;
		case Level2:	newOption = Level3;
						break;
		case Level3:	newOption = Level4;
						break;
		case Level4:	newOption = Level5;
						break;
		case Level5:	newOption = Level1;
						break;
		}
	}
	else
	{
		switch (currentOption)
		{
		case Level1:	newOption = Level5;
						break;
		case Level2:	newOption = Level1;
						break;
		case Level3:	newOption = Level2;
						break;
		case Level4:	newOption = Level3;
						break;
		case Level5:	newOption = Level4;
						break;
		}
	}
	alt_up_char_buffer_string(charBuffer, ">", MenuCaretX, newOption);
	return newOption;
}

MenuOptions changeMenuOption(alt_up_char_buffer_dev* charBuffer, MenuOptions currentOption, int direction)
{
	alt_up_char_buffer_string(charBuffer, " ", MenuCaretX, currentOption);
	MenuOptions newOption = MenuPlayGame;
	if (direction < 0)
	{
		switch (currentOption)
		{
		case MenuPlayGame: 		newOption = MenuLevelSelect;
								break;
		case MenuLevelSelect: 	newOption = MenuHighScores;
								break;
		case MenuHighScores: 	newOption = MenuPlayGame;
								break;
		}
	}
	else
	{
		switch (currentOption)
		{
		case MenuPlayGame: 		newOption = MenuHighScores;
								break;
		case MenuLevelSelect: 	newOption = MenuPlayGame;
								break;
		case MenuHighScores: 	newOption = MenuLevelSelect;
								break;
		}
	}
	alt_up_char_buffer_string(charBuffer, ">", MenuCaretX, newOption);
	return newOption;
}

LevelOptions showLevelSelectMenu(alt_up_char_buffer_dev* charBuffer, LevelOptions currentLevel, float MenuLoopClockTickCount)
{
	printLevelSelect(charBuffer, currentLevel);
	int isButtonPressed = 1;
	while(1)
	{
		alt_timestamp_start();
		if (isButtonActive(3)) //move up
		{
			if (!isButtonPressed)
			{
				isButtonPressed = 1;
				currentLevel = changeLevelOption(charBuffer, currentLevel, 1);
			}
		}
		else if (isButtonActive(2)) //move down
		{
			if (!isButtonPressed)
			{
				isButtonPressed = 1;
				currentLevel = changeLevelOption(charBuffer, currentLevel, -1);
			}
		}
		else if (isButtonActive(1)) //select menu item
		{
			if (!isButtonPressed)
			{
				return currentLevel;
			}
		}
		else
		{
			isButtonPressed = 0;
		}

		while ((float) (alt_timestamp()) < MenuLoopClockTickCount);
	}
}

HighScoreEntry* createNewEntry(char *name, int score)
{
	HighScoreEntry* Entry = (HighScoreEntry *)malloc(sizeof(HighScoreEntry));
	Entry->name = name;
	Entry->score = score;
	return Entry;
}

void removeLastEntry(HighScoreEntry* head)
{
	HighScoreEntry *curNode = head;
	HighScoreEntry **prevNode = &(head->nextEntry);

	while(curNode->nextEntry != NULL)
	{
		//Iteration
		prevNode = &(curNode->nextEntry);
		curNode = curNode->nextEntry;
	}

	free(curNode->name);
	free(curNode);
	curNode = NULL;
	(*prevNode) = NULL;

}

HighScoreEntry* recalculateHighScores(alt_up_char_buffer_dev* charBuffer, HighScoreEntry *highScores, int playerScore, LevelOptions currentLevelOption)
{
	//HighScoreListSize=5
	HighScoreEntry* newEntry;
	char * newName;

	if(playerScore < 0)
		return highScores;

	HighScoreEntry *head = highScores;
	HighScoreEntry **prevNode = &(highScores);

	//New Highest Score
	if(playerScore > head->score)
	{
		newName = getPlayerName(charBuffer);
		newEntry = createNewEntry(newName,playerScore);
		newEntry->nextEntry = head;
		removeLastEntry(newEntry);
		saveHighScores(newEntry, currentLevelOption);
		return newEntry;
	}

	//Iteration
	prevNode = &(head->nextEntry);
	head = head->nextEntry;

	//New top 2-5 scores
	while(head != NULL)
	{
		if(playerScore > head->score)
		{
			newName = getPlayerName(charBuffer);
			newEntry = createNewEntry(newName,playerScore);

			//Weaving
			newEntry->nextEntry = head;
			(*prevNode) = newEntry;

			removeLastEntry(newEntry);
			saveHighScores(highScores, currentLevelOption);
			return highScores;
		}

		//Iteration
		prevNode = &(head->nextEntry);
		head = head->nextEntry;
	}
	return highScores;
}

void releaseHighScores(HighScoreEntry* head)
{
	HighScoreEntry* prev = head;
	if(head != NULL)
	{
		head = head->nextEntry;
		//Free memory
		free(prev->name);
		free(prev);

		prev = head;
	}
}

void showMenu(alt_up_char_buffer_dev *char_buffer, alt_up_pixel_buffer_dma_dev* pxBuffer, alt_up_audio_dev * audio, float GameLoopClockTickCount)
{
	float MenuLoopClockTickCount = GameLoopClockTickCount * 100;

	LevelOptions currentLevelOption = Level1;
	MenuOptions currentMenuSelect = MenuPlayGame;
    HighScoreEntry* highScores = loadHighScores(currentLevelOption);

	printMenuOptions(char_buffer, currentMenuSelect);

	int isButtonPressed = 0;
	while(1) {
		alt_timestamp_start();
		if (isButtonActive(3)) //move up
		{
			if (!isButtonPressed)
			{
				isButtonPressed = 1;
				currentMenuSelect = changeMenuOption(char_buffer, currentMenuSelect, 1);
			}
		}
		else if (isButtonActive(2)) //move down
		{
			if (!isButtonPressed)
			{
				isButtonPressed = 1;
				currentMenuSelect = changeMenuOption(char_buffer, currentMenuSelect, -1);
			}
		}
		else if (isButtonActive(1)) //select menu item
		{
			if (!isButtonPressed)
			{
				isButtonPressed = 1;
				switch (currentMenuSelect)
				{
					case MenuPlayGame:
					{
						int playerScore, logAmmo = 0;
						int levelConfig[GameHeight];
						readLevel(currentLevelOption, levelConfig, &logAmmo);
						playerScore = playGame(char_buffer, pxBuffer, audio, levelConfig, logAmmo, GameLoopClockTickCount);

							//nathan replace this with re-weaving
						highScores = recalculateHighScores(char_buffer, highScores, playerScore, currentLevelOption);

						alt_up_pixel_buffer_dma_clear_screen(pxBuffer, 0);
						printMenuOptions(char_buffer, currentMenuSelect);
						break;
					}
					case MenuLevelSelect:
					{
						currentLevelOption = showLevelSelectMenu(char_buffer, currentLevelOption, MenuLoopClockTickCount);
						releaseHighScores(highScores);
						highScores = loadHighScores(currentLevelOption);
						printMenuOptions(char_buffer, currentMenuSelect);
						break;
					}
					case MenuHighScores:
					{
						printHighScores(char_buffer, highScores, MenuLoopClockTickCount,currentLevelOption);
						printMenuOptions(char_buffer, currentMenuSelect);
						break;
					}
				} //end currentMenuSelect switch
			}
		}
		else
		{
			isButtonPressed = 0;
		}

		while ((float) (alt_timestamp()) < MenuLoopClockTickCount);
	}
}

void fillLevel(int level[], int rows) {
	int i;
	for (i=rows; i < GameHeight; i++) {
		level[i] = 0;
	}
}

int enumLeveltoIntLevel(LevelOptions currentLevelOption)
{
	int levelNum;
	switch (currentLevelOption)
		{
		case Level1: 	levelNum = 1;
						break;
		case Level2: 	levelNum = 2;
						break;
		case Level3: 	levelNum = 3;
						break;
		case Level4: 	levelNum = 4;
						break;
		case Level5: 	levelNum = 5;
						break;
		}
	return levelNum;
}

void readLevel(LevelOptions currentLevelOption, int level[], int * logAmmo) {

	int levelNum = enumLeveltoIntLevel(currentLevelOption);

	alt_up_sd_card_dev *device_reference = NULL;
	int connected = 0;

	device_reference = alt_up_sd_card_open_dev(SD_CARD_NAME);
	if (device_reference != NULL) {
		if ((connected == 0) && (alt_up_sd_card_is_Present())) {
			if (alt_up_sd_card_is_FAT16()) {

				char levelName[10];
				sprintf(levelName, "L%02d.txt", levelNum);

				short int fileHandle = alt_up_sd_card_fopen(levelName, false);
				if(fileHandle >= 0){
					short int byteRead;
					int x = 0;
					char buf[3];
					int element=0;
					int bufPos=0;
					while((byteRead = alt_up_sd_card_read(fileHandle)) != -1)
					{
						x++;
						if (byteRead==',' || byteRead==';') {
							if (element == 0) {
								*logAmmo = 10*((int)buf[0]-0x30) + ((int)buf[1]-0x30);
							}
							else {
								if (buf[0]=='-') {
									level[element-1]=-((int)buf[1]-0x30);
								}
								else {
									level[element-1]=((int)buf[0]-0x30);
								}
							}
							element++;
							bufPos=0;
						}
						else {
							buf[bufPos]=byteRead;
							bufPos++;
						}
					}
					fillLevel(level, element-2);
					alt_up_sd_card_fclose(fileHandle);
				}
			} else {
				printf("Unknown file system.\n");
			}
			connected = 1;

		} else if ((connected == 1) && (alt_up_sd_card_is_Present() == false)) {
			printf("Card disconnected.\n");
			connected = 0;
		}
	}
}

void init_keyboard(void) {
	ps2_device = alt_up_ps2_open_dev(PS2_0_NAME);
	alt_up_ps2_init(ps2_device);
}

void printHighScores(alt_up_char_buffer_dev *charBuffer, HighScoreEntry* highScores, float MenuLoopClockTickCount,LevelOptions currentLevelOption)
{
	int i;
	char title[21];
	alt_up_char_buffer_clear(charBuffer);
	int cursorX = MenuOptionX;
	int cursorY = 20;
	int levelNum = enumLeveltoIntLevel(currentLevelOption);

	HighScoreEntry* currentEntry = highScores;

	sprintf(title, "High Scores: Level %d", levelNum);
	alt_up_char_buffer_string(charBuffer, title, cursorX, cursorY);
	for (i = 0; i < HighScoreListSize && currentEntry != NULL; ++i)
	{
		cursorY += 2;

		char scoreString[6];
		sprintf(scoreString, "%d", currentEntry->score % 10000);

		alt_up_char_buffer_string(charBuffer, currentEntry->name, cursorX, cursorY);

		alt_up_char_buffer_string(charBuffer, scoreString, cursorX + HighScoreNameSize + 1, cursorY);

		currentEntry = currentEntry->nextEntry;
	}


	int isButtonPressed = 1;
	while(1)
	{
		alt_timestamp_start();

		if (	(isButtonActive(1))
			||	(isButtonActive(2))
			||	(isButtonActive(3))
			)
		{
			if (!isButtonPressed)
				return;
		}
		else
		{
			isButtonPressed = 0;
		}

		while ((float) (alt_timestamp()) < MenuLoopClockTickCount);
	}
}

char* getPlayerName(alt_up_char_buffer_dev *charBuffer)
{
	alt_up_char_buffer_string(charBuffer, "Enter Player Name:", 20, 20);

	char* newName = (char*)malloc(sizeof(char) * (HighScoreNameSize+1));

	int i = 0;
	while(i < HighScoreNameSize) {
		alt_timestamp_start();
		while(alt_timestamp() < alt_timestamp_freq() / 4);
		char c = get_key_from_keyboard();
		if (c >= 'A' && c <= 'Z') {
			printf("char found: %c\n", c);
			char nameLetter[2];
			sprintf(nameLetter, "%c", c);
			alt_up_char_buffer_string(charBuffer, nameLetter, 40 + i, 20);
			newName[i++] = c;
		}
	}
	newName[HighScoreNameSize] = '\0';
	return newName;
}

char* generatePlayerName()
{
	//fill playerName with no more than HighScoreNameSize chars, including null termination
	static int playerCount = 0;
	char* newName = (char*)malloc(sizeof(char) * (13));
	sprintf(newName, "P%d", ++playerCount % 100);
	return newName;
}

void writeNameSD(char *name, short int fileHandle)
{
	int i;
	for(i=0;name[i]!='\0';i++)
	{
		bool result = alt_up_sd_card_write(fileHandle,name[i]);
		if (!result)
			printf("writeFailed: %c\n", name[i]);
	}
}

void writeScoreSD(int score, short int fileHandle)
{
	if(score < 10)
	{
		char num[2];
		alt_up_sd_card_write(fileHandle,'0');
		sprintf(num,"%01d", score);
		alt_up_sd_card_write(fileHandle,(char)num[0]);
	}
	else
	{
		char num[2];
		sprintf(num,"%01d", score/10);
		alt_up_sd_card_write(fileHandle,(char)num[0]);
		sprintf(num,"%01d", score%10);
		alt_up_sd_card_write(fileHandle,(char)num[0]);
	}
}

void saveHighScores(HighScoreEntry *head, LevelOptions currentLevelOption)
{
	int levelNum = enumLeveltoIntLevel(currentLevelOption);
	alt_up_sd_card_dev *device_reference = NULL;
	device_reference = alt_up_sd_card_open_dev(SD_CARD_NAME);
		if (device_reference != NULL) {
			if ((alt_up_sd_card_is_Present())) {
				if (alt_up_sd_card_is_FAT16()) {

					char filename[7];
					sprintf(filename,"H%d.txt", levelNum);
					short int fileHandle = alt_up_sd_card_fopen(filename, false);
					if(fileHandle >= 0){

						while(head != NULL)
						{
							printf("writing name \"%s\" with score %d\n", head->name, head->score);
							writeNameSD(head->name, fileHandle);
							alt_up_sd_card_write(fileHandle,',');
							writeScoreSD(head->score, fileHandle);
							alt_up_sd_card_write(fileHandle,';');

							//iterate
							head = head->nextEntry;
						}

					}
					alt_up_sd_card_fclose(fileHandle);
				}
			}
		}
}

HighScoreEntry* loadHighScores(LevelOptions currentLevelOption)
{
	int levelNum = enumLeveltoIntLevel(currentLevelOption);
	alt_up_sd_card_dev *device_reference = NULL;
	HighScoreEntry* curNode;
	HighScoreEntry* head;

	device_reference = alt_up_sd_card_open_dev(SD_CARD_NAME);
	if (device_reference != NULL) {
		if ((alt_up_sd_card_is_Present())) {
			if (alt_up_sd_card_is_FAT16()) {

				char filename[7];
				sprintf(filename,"H%d.txt", levelNum);
				short int fileHandle = alt_up_sd_card_fopen(filename, false);
				if(fileHandle >= 0){
					short int byteRead;
					int x = 0;
					char buf[3];
					int topScore=0;
					int bufPos=0;
					while((byteRead = alt_up_sd_card_read(fileHandle)) != -1)
					{
						x++;
						if (byteRead==';')
						{
							//End of score
							curNode->score = 10*((int)buf[0]-0x30) + ((int)buf[1]-0x30);
							printf("%d\n",curNode->score);
							bufPos=0;
						}
						else if (byteRead==',')
						{
							//End of Name
							HighScoreEntry *newEntry = (HighScoreEntry *)malloc(sizeof(HighScoreEntry));
							newEntry->name = (char *)malloc(4*sizeof(char));

							//Assign Name
							newEntry->name[0] = buf[0];
							newEntry->name[1] = buf[1];
							newEntry->name[2] = buf[2];
							newEntry->name[3] = '\0';

							if(topScore==0)
							{
								head=newEntry;
								curNode=newEntry;
								topScore = 1;
							}
							else
							{
								curNode->nextEntry = newEntry;
								curNode = newEntry;
							}
							bufPos=0;
						}
						else{
							//Reading data
							buf[bufPos]=byteRead;
							bufPos++;
						}
					}
					curNode->nextEntry = NULL;
					alt_up_sd_card_fclose(fileHandle);
					return head;
				}
			}
		}
	}
	return NULL;
}

int main(void)
{
	init_keyboard();
	av_config_setup();
	alt_up_audio_dev * audio = alt_up_audio_open_dev(AUDIO_NAME);
	alt_up_audio_reset_audio_core(audio);
	alt_up_pixel_buffer_dma_dev* pxBuffer = initVGA();
	alt_up_char_buffer_dev *char_buffer;
	char_buffer = alt_up_char_buffer_open_dev("/dev/char_drawer");
	alt_up_char_buffer_init(char_buffer);
	alt_up_char_buffer_clear(char_buffer);

    float GameLoopClockTickCount = alt_timestamp_freq() * (float)GameLoopTimeMs / 1000.0;

	showMenu(char_buffer, pxBuffer, audio, GameLoopClockTickCount);
	return 0;
}

