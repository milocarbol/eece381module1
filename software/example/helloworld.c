#include "altera_up_avalon_video_pixel_buffer_dma.h"
#include "sys/alt_timestamp.h"
#include "system.h"
#include <stdio.h>

void initializeGame(alt_up_pixel_buffer_dma_dev* pxBuffer);
void drawGameBoard(alt_up_pixel_buffer_dma_dev* pxBuffer);
void calculateLogMovement();
void calculateFrogMovement();
void updateScreen();
void addLog(int row, int numLogs);

typedef enum
{
	Cell_Grass,
	Cell_Water,
	Cell_Log,
	Cell_FroggerOnLog,
	Cell_FroggerOnGrass,
	Cell_FroggerInWater
} Cell;

void drawCell(alt_up_pixel_buffer_dma_dev* pxBuffer, Cell cell, int x, int y);
void drawColouredCell(alt_up_pixel_buffer_dma_dev* pxBuffer, unsigned int color, int x, int y);
alt_up_pixel_buffer_dma_dev* initVGA();


enum
{
	CellSideLength = 10, //don't toggle
	GameWidth = 32, //don't toggle
	MaxGameHeight = 24 //don't toggle
};

enum
{
	GameHeight = 6,
	GameLoopTimeMs = 500
};

Cell GameBoard[GameHeight][GameWidth];
const int RiverSpeeds[GameHeight] = {0, -1, -1, 1, -1, 0};
int WaitingLogs[GameHeight] = {0, 3};

int main(void)
{
	const float GameLoopClockTickCount = alt_timestamp_freq() * (float)GameLoopTimeMs / 1000.0;
	alt_up_pixel_buffer_dma_dev* pixel_buffer = initVGA();
	
	initializeGame(pixel_buffer);
	
	while(1)
	{
		alt_timestamp_start();

		calculateLogMovement();

		drawGameBoard(pixel_buffer);
		while ((float) (alt_timestamp()) < GameLoopClockTickCount);
	}


}

void initializeGame(alt_up_pixel_buffer_dma_dev* pxBuffer)
{
	int h, w;
	for (h = 0; h < GameHeight; ++h)
	{
		for (w = 0; w < GameWidth; ++w)
		{
			GameBoard[h][w] = RiverSpeeds[h] ? Cell_Water : Cell_Grass;
		}
	}
	GameBoard[0][GameWidth/2] = Cell_FroggerOnGrass;
	
	drawGameBoard(pxBuffer);
}

void drawGameBoard(alt_up_pixel_buffer_dma_dev* pxBuffer)
{
	int h, w;
	for (h = 0; h < GameHeight; ++h)
	{
		for (w = 0; w < GameWidth; ++w)
		{
			drawCell(pxBuffer, GameBoard[h][w], w, h);
		}
	}
}

void calculateLogMovement()
{
	int i, j;
	for (i = 0; i < GameHeight; ++i)
	{
		if (RiverSpeeds[i] > 0)
		{
			for (j = GameWidth - 1; j >=0; --j)
			{
				GameBoard[i][j] = GameBoard[i][j];
			}
			for (j = 0; j < RiverSpeeds[i]; ++j)
			{
				if (WaitingLogs[i])
				{
					GameBoard[i][j] = Cell_Log;
					--WaitingLogs[i];
				}
				else
				{
					GameBoard[i][j] = Cell_Water;
				}
			}
		}
		else if (RiverSpeeds[i] < 0)
		{
			for (j = -RiverSpeeds[i]; j < GameWidth; ++j)
			{
				GameBoard[i][j + RiverSpeeds[i]] = GameBoard[i][j];
			}
			for (j = GameWidth - 1; j >= GameWidth + RiverSpeeds[i]; --j)
			{
				if (WaitingLogs[i])
				{
					GameBoard[i][j] = Cell_Log;
					--WaitingLogs[i];
				}
				else
				{
					GameBoard[i][j] = Cell_Water;
				}
			}
		}
	}
}

void calculateFrogMovement()
{
}

void updateScreen()
{

}

void addLog(int row, int logSize)
{
	if (row < GameHeight)
		WaitingLogs[row] += logSize;
}

alt_up_pixel_buffer_dma_dev* initVGA() {
	alt_up_pixel_buffer_dma_dev* pixel_buffer;
	// Use the name of your pixel buffer DMA core
	pixel_buffer = alt_up_pixel_buffer_dma_open_dev("/dev/pixel_buffer_dma");
	// Set the background buffer address – Although we don’t use the background,
	// they only provide a function to change the background buffer address, so
	// we must set that, and then swap it to the foreground.
	alt_up_pixel_buffer_dma_change_back_buffer_address(pixel_buffer, PIXEL_BUFFER_BASE);
	// Swap background and foreground buffers
	alt_up_pixel_buffer_dma_swap_buffers(pixel_buffer);
	// Wait for the swap to complete
	while
		(alt_up_pixel_buffer_dma_check_swap_buffers_status(pixel_buffer));
	// Clear the screen
	alt_up_pixel_buffer_dma_clear_screen(pixel_buffer, 0);
	return pixel_buffer;
}

void drawCell(alt_up_pixel_buffer_dma_dev* pxBuffer, Cell cell, int x, int y)
{
	enum
	{
		Colors_Frogger = 0x0F00,
		Colors_Log = 0x5922,
		Colors_Water = 0x00FF,
		Colors_Grass = 0x3AE1,
		Colors_Error = 0xF800
	};
	
	unsigned int cellColor = Colors_Error;
	
	switch (GameBoard[y][x])
	{
		case Cell_Grass			: cellColor = Colors_Grass;
								  break;
		case Cell_Log			: cellColor = Colors_Log;
								  break;
		case Cell_Water			: cellColor = Colors_Water;
								  break;
		case Cell_FroggerOnGrass:
		case Cell_FroggerInWater:
		case Cell_FroggerOnLog	: cellColor = Colors_Frogger;
								  break;
	}
	drawColouredCell(pxBuffer, (unsigned int)cellColor, x, y);
}

void drawColouredCell(alt_up_pixel_buffer_dma_dev* pxBuffer, unsigned int color, int x, int y)
{
	static RowOffset = 0;//(MaxGameHeight - GameHeight)/2;
	int i, j;
	int yMin = CellSideLength * (y + RowOffset);
	int yMax = yMin + CellSideLength;
	int xMin = CellSideLength * x;
	int xMax = CellSideLength * (x + 1);
	
	for (i = yMin; i < yMax; ++i)
	{
		for (j = xMin; j < xMax; ++j)
		{
			alt_up_pixel_buffer_dma_draw(pxBuffer, color, j, i);
		}
	}
}
